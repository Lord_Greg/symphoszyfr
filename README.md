# Symphoszyfr #
Aplikacja szyfrująca ciągi tekstu, przy pomocy algorytmu 3DES. Jako klucz wykorzystuje zaznaczone przez użytkownika fragmenty odtwarzanego pliku mp3.

Aplikacja zaliczeniowa z przedmiotu "Bezpieczeństwo systemów komputerowych" - Czerwiec 2016.