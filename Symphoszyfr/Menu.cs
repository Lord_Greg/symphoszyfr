﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.IO;


namespace Symphoszyfr
{
    public partial class Menu : Form
    {
        WindowsMediaPlayer player;
        Timer timer, timerMax;
        bool czyMuzykaGra, czyUżytkownikDziała;
        string ścieżkaPliku;
        Klucz klucz;


        public Menu()
        {
            InitializeComponent();

            klucz = new Klucz();
            lblNazwaPioseki.Text = "";

            czyUżytkownikDziała = false;
            czyMuzykaGra = false;
            player = new WindowsMediaPlayer();
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += new EventHandler(timer_Tick);
        }

        
        #region Zdarzenia Kontrolek

        private void btnWczytajPiosenke_Click(object sender, EventArgs e)
        {
            playerStop();
            resetujKlucz();

            OpenFileDialog ofdWczytajPlik = new OpenFileDialog();
            ofdWczytajPlik.DefaultExt = ".mp3";
            ofdWczytajPlik.Filter = "Piliki mp3 |*.mp3";
            ofdWczytajPlik.ShowDialog();

            if (ofdWczytajPlik.FileName != "")
            {
                ścieżkaPliku = ofdWczytajPlik.FileName;
                
                player.URL = ścieżkaPliku;
                player.controls.pause();
                player.settings.volume = 50;

                timerMax = new Timer(); //opóźnienie pewnych działań do czasu wczytania piosenki
                timerMax.Interval = 500;
                timerMax.Tick += new EventHandler(timerMax_Tick);
                timerMax.Start();

                tbCzas.Value = 0;
                czyMuzykaGra = false;

                
                StringBuilder strbNazwaPliku = new StringBuilder(ofdWczytajPlik.SafeFileName);
                strbNazwaPliku.Remove(strbNazwaPliku.Length - 4, 4);
                lblNazwaPioseki.Text = strbNazwaPliku.ToString();

                klucz = new Klucz(strbNazwaPliku.ToString(), File.ReadAllBytes(ścieżkaPliku));

                resetujKlucz();
            }
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            if (ścieżkaPliku != "")
            {
                if (czyMuzykaGra)
                {
                    playerPauza();
                }
                else
                {
                    playerStart();
                }
            }
        }
        
        private void btnStop_Click(object sender, EventArgs e)
        {
            playerStop();
        }


        private void btnZłapKlucz_Click(object sender, EventArgs e)
        {
            klucz.WartościKlucza.Add(tbCzas.Value);
            lblKlucz.Text += lblCzas.Text + " ";
        }

        private void btnCofnij_Click(object sender, EventArgs e)
        {
            if (klucz.WartościKlucza.Count > 0)
            {
                klucz.WartościKlucza.RemoveAt(klucz.WartościKlucza.Count - 1);

                StringBuilder strb = new StringBuilder(lblKlucz.Text);
                strb.Remove(lblKlucz.Text.Length - 6, 6);
                lblKlucz.Text = strb.ToString();
            }
        }


        private void btnSzyfruj_Click(object sender, EventArgs e)
        {
            if (rtbTekstDoSzyfrowania.Text != "")
            {
                if (klucz.NazwaPiosenki != "")
                {
                    rtbTekstDoSzyfrowania.Text = Szyfrowanie.szyfruj_3DES(rtbTekstDoSzyfrowania.Text, klucz.WartośćKlucza);
                }
                else
                {
                    MessageBox.Show("Wczytaj piosenkę");
                }
            }
            else
            {
                MessageBox.Show("Wpisz jakąś wiadomość");
            }
        }

        private void btnDeszyfruj_Click(object sender, EventArgs e)
        {
            if (rtbTekstDoSzyfrowania.Text != "")
            {
                if (klucz.NazwaPiosenki != "")
                {
                    rtbTekstDoSzyfrowania.Text = Szyfrowanie.deszyfruj_3DES(rtbTekstDoSzyfrowania.Text, klucz.WartośćKlucza);
                }
                else
                {
                    MessageBox.Show("Wczytaj piosenkę");
                }
            }
            else
            {
                MessageBox.Show("Wpisz jakąś wiadomość");
            }
        }


        private void btnWczytajPlik_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdWczytajTekst = new OpenFileDialog();
            ofdWczytajTekst.DefaultExt = ".txt";
            ofdWczytajTekst.Filter = "Piliki txt |*.txt";
            ofdWczytajTekst.ShowDialog();

            if (ofdWczytajTekst.FileName != "")
            {
                rtbTekstDoSzyfrowania.Text = File.ReadAllText(ofdWczytajTekst.FileName);
            }
        }

        private void btnZapiszDoPliku_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdZapiszTekst = new SaveFileDialog();
            sfdZapiszTekst.DefaultExt = ".txt";
            sfdZapiszTekst.Filter = "Piliki txt |*.txt";
            sfdZapiszTekst.ShowDialog();

            if (sfdZapiszTekst.FileName != "")
            {
                File.WriteAllText(sfdZapiszTekst.FileName, rtbTekstDoSzyfrowania.Text);
            }
        }


        private void btnZapiszKlucz_Click(object sender, EventArgs e)
        {
            if (klucz.NazwaPiosenki != "")
            {
                SaveFileDialog sfdZapiszKlucz = new SaveFileDialog();
                sfdZapiszKlucz.DefaultExt = ".txt";
                sfdZapiszKlucz.Filter = "Piliki txt |*.txt";
                sfdZapiszKlucz.ShowDialog();

                if (sfdZapiszKlucz.FileName != "")
                {
                    string kluczDoZapisu;
                    kluczDoZapisu = lblNazwaPioseki.Text;

                    StringBuilder strb = new StringBuilder(lblKlucz.Text);
                    strb.Remove(0, 7);

                    kluczDoZapisu += "\n" + strb.ToString();

                    File.WriteAllText(sfdZapiszKlucz.FileName, kluczDoZapisu);
                }
            }
            else
            {
                MessageBox.Show("Wczytaj jakąś piosenkę!");
            }
        }


        private void tbCzas_MouseUp(object sender, MouseEventArgs e)
        {
            czyUżytkownikDziała = false;
        }

        private void tbCzas_MouseDown(object sender, MouseEventArgs e)
        {
            czyUżytkownikDziała = true;
        }


        private void tbCzas_Scroll(object sender, EventArgs e)
        {
            if (czyUżytkownikDziała)
            {
                player.controls.currentPosition = Convert.ToDouble(tbCzas.Value);
                if (czyMuzykaGra)
                {
                    player.controls.play();
                }
                else
                {
                    player.controls.pause();
                }
            }
        }


        private void tbCzas_ValueChanged(object sender, EventArgs e)
        {
            lblCzas.Text = podajCzas();
        }

        #endregion

        private void playerStart()
        {
            czyMuzykaGra = true;
            player.controls.play();
            timer.Start();
            btnStart.Text = "Pauza";
        }

        private void playerPauza()
        {
            czyMuzykaGra = false;
            player.controls.pause();
            timer.Stop();
            btnStart.Text = "Start";
        }

        private void playerStop()
        {
            player.controls.stop();
            timer.Stop();
            tbCzas.Value = 0;
            lblCzas.Text = "00:00";
            btnStart.Text = "Start";
        }


        private void resetujKlucz()
        {
            lblKlucz.Text = "Klucz: ";
            klucz.WartościKlucza.Clear();
        }

        private string podajCzas()
        {
            string czas = "";
            int liczba;

            liczba = tbCzas.Value / 60;
            if (liczba < 10) czas += "0";
            czas += liczba.ToString();

            czas += ":";

            liczba = tbCzas.Value % 60;
            if (liczba < 10) czas += "0";
            czas += liczba.ToString();

            return czas;
        }

        

        private void timer_Tick(object sender, EventArgs e)
        {
            if (tbCzas.Value + 1 <= tbCzas.Maximum && !czyUżytkownikDziała)
            {
                tbCzas.Value++;
            }
        }

        private void timerMax_Tick(object sender, EventArgs e)
        {
            timerMax.Stop();

            int ilośćSekundPiosenki = Convert.ToInt32(player.controls.currentItem.duration);
            if (ilośćSekundPiosenki != 0)
            {
                tbCzas.Maximum = ilośćSekundPiosenki;
                klucz.zmieńDługośćBlokuKlucza(ilośćSekundPiosenki);
            }
            else
            {
                player.controls.stop();
                MessageBox.Show("Wystąpił błąd podczas wczytywania piosenki. Spróbuj ponownie.");
            }

            player.controls.stop();
        }

    }
}
