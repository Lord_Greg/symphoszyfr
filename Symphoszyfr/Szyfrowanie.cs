﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;

namespace Symphoszyfr
{
    class Szyfrowanie
    {
        private static TripleDES nowy_3DES(string klucz)
        {
            MD5 hash_MD5 = new MD5CryptoServiceProvider();
            TripleDES nowy3DES = new TripleDESCryptoServiceProvider();
            nowy3DES.Key = hash_MD5.ComputeHash(Encoding.Unicode.GetBytes(klucz));
            nowy3DES.IV = new byte[nowy3DES.BlockSize / 8];
            return nowy3DES;
        }

        public static string szyfruj_3DES(string tekstJawny, string klucz)
        {
            TripleDES des = nowy_3DES(klucz);
            ICryptoTransform cryptoTransformator = des.CreateEncryptor();
            byte[] bufor = Encoding.Unicode.GetBytes(tekstJawny);

            return Convert.ToBase64String( cryptoTransformator.TransformFinalBlock(bufor, 0, bufor.Length) );
        }

        public static string deszyfruj_3DES(string tekstTajny, string klucz)
        {
            string tekstZwrotny = "";

            try
            {
                byte[] bufor = Convert.FromBase64String(tekstTajny);
                TripleDES des = nowy_3DES(klucz);
                ICryptoTransform cryptoTransformator = des.CreateDecryptor();
                byte[] output = cryptoTransformator.TransformFinalBlock(bufor, 0, bufor.Length);

                tekstZwrotny = Encoding.Unicode.GetString(output);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Wystąpił błąd: " + ex.Message);
                tekstZwrotny = tekstTajny;
            }

            return tekstZwrotny;
        }

        
    }
}