﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symphoszyfr
{
    class Klucz
    {
        private string nazwaPiosenki;
        private List<int> wartościKlucza;
        private byte[] piosenkaBajty;
        private int długośćBlokuKlucza;

        public string NazwaPiosenki
        {
            get
            {
                return nazwaPiosenki;
            }
            set
            {
                nazwaPiosenki = value;
            }
        }
        public List<int> WartościKlucza
        {
            get
            {
                return wartościKlucza;
            }

            set
            {
                wartościKlucza = value;
            }
        }
        public string WartośćKlucza
        {
            get
            {
                string klucz = "";
                StringBuilder strb = new StringBuilder(nazwaPiosenki);

                for (int i = 0; i <= 10; i += 2)
                {
                    double któryZnak = i;
                    któryZnak /= 10;
                    któryZnak *= (strb.Length - 1);
                    klucz += strb.ToString(Convert.ToInt32(któryZnak) , 1);
                }
                if (wartościKlucza.Count > 0)
                {
                    if (długośćBlokuKlucza == 0) długośćBlokuKlucza = 1;

                    foreach (int wartość in wartościKlucza)
                    {
                        for (int i = 0; i < długośćBlokuKlucza; i++)
                        {
                            klucz += piosenkaBajty[wartość * długośćBlokuKlucza + i].ToString();
                        }
                    }
                }
                for (int i = 1; i <= 10; i += 2)
                {
                    double któryZnak = i;
                    któryZnak /= 10;
                    któryZnak *= (strb.Length - 1);
                    klucz += strb.ToString(Convert.ToInt32(któryZnak) , 1);
                }
                return klucz;
            }
        }


        public Klucz()
        {
            NazwaPiosenki = "";
            WartościKlucza = new List<int>();
            this.piosenkaBajty = new byte[0];
            długośćBlokuKlucza = 0;
        }

        public Klucz(string nazwaPiosenki, byte[] piosenkaBajty)
        {
            NazwaPiosenki = nazwaPiosenki;
            WartościKlucza = new List<int>();
            this.piosenkaBajty = new byte[piosenkaBajty.GetLength(0)];

            for (int i = 0; i < piosenkaBajty.GetLength(0); i++)
            {
                this.piosenkaBajty[i] = piosenkaBajty[i];
            }

            długośćBlokuKlucza = 1;
        }


        public void zmieńDługośćBlokuKlucza(int ilośćSekundPiosenki)
        {
            długośćBlokuKlucza = piosenkaBajty.GetLength(0) / ilośćSekundPiosenki;
        }
    }
}
