﻿namespace Symphoszyfr
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCzas = new System.Windows.Forms.TrackBar();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnWczytajPiosenke = new System.Windows.Forms.Button();
            this.btnZłapKlucz = new System.Windows.Forms.Button();
            this.lblKlucz = new System.Windows.Forms.Label();
            this.lblCzas = new System.Windows.Forms.Label();
            this.rtbTekstDoSzyfrowania = new System.Windows.Forms.RichTextBox();
            this.btnSzyfruj = new System.Windows.Forms.Button();
            this.btnZapiszDoPliku = new System.Windows.Forms.Button();
            this.btnWczytajPlik = new System.Windows.Forms.Button();
            this.btnDeszyfruj = new System.Windows.Forms.Button();
            this.btnCofnij = new System.Windows.Forms.Button();
            this.lblNazwaPioseki = new System.Windows.Forms.Label();
            this.btnZapiszKlucz = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbCzas)).BeginInit();
            this.SuspendLayout();
            // 
            // tbCzas
            // 
            this.tbCzas.Location = new System.Drawing.Point(32, 100);
            this.tbCzas.Maximum = 0;
            this.tbCzas.Name = "tbCzas";
            this.tbCzas.Size = new System.Drawing.Size(210, 45);
            this.tbCzas.TabIndex = 0;
            this.tbCzas.Scroll += new System.EventHandler(this.tbCzas_Scroll);
            this.tbCzas.ValueChanged += new System.EventHandler(this.tbCzas_ValueChanged);
            this.tbCzas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tbCzas_MouseDown);
            this.tbCzas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbCzas_MouseUp);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(94, 151);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(60, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(32, 151);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(56, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnWczytajPiosenke
            // 
            this.btnWczytajPiosenke.Location = new System.Drawing.Point(225, 145);
            this.btnWczytajPiosenke.Name = "btnWczytajPiosenke";
            this.btnWczytajPiosenke.Size = new System.Drawing.Size(59, 35);
            this.btnWczytajPiosenke.TabIndex = 4;
            this.btnWczytajPiosenke.Text = "Wczytaj Piosenkę";
            this.btnWczytajPiosenke.UseVisualStyleBackColor = true;
            this.btnWczytajPiosenke.Click += new System.EventHandler(this.btnWczytajPiosenke_Click);
            // 
            // btnZłapKlucz
            // 
            this.btnZłapKlucz.Location = new System.Drawing.Point(65, 31);
            this.btnZłapKlucz.Name = "btnZłapKlucz";
            this.btnZłapKlucz.Size = new System.Drawing.Size(75, 23);
            this.btnZłapKlucz.TabIndex = 5;
            this.btnZłapKlucz.Text = "Złap Klucz";
            this.btnZłapKlucz.UseVisualStyleBackColor = true;
            this.btnZłapKlucz.Click += new System.EventHandler(this.btnZłapKlucz_Click);
            // 
            // lblKlucz
            // 
            this.lblKlucz.AutoSize = true;
            this.lblKlucz.Location = new System.Drawing.Point(12, 9);
            this.lblKlucz.Name = "lblKlucz";
            this.lblKlucz.Size = new System.Drawing.Size(39, 13);
            this.lblKlucz.TabIndex = 6;
            this.lblKlucz.Text = "Klucz: ";
            // 
            // lblCzas
            // 
            this.lblCzas.AutoSize = true;
            this.lblCzas.Location = new System.Drawing.Point(34, 132);
            this.lblCzas.Name = "lblCzas";
            this.lblCzas.Size = new System.Drawing.Size(34, 13);
            this.lblCzas.TabIndex = 7;
            this.lblCzas.Text = "00:00";
            // 
            // rtbTekstDoSzyfrowania
            // 
            this.rtbTekstDoSzyfrowania.Location = new System.Drawing.Point(290, 54);
            this.rtbTekstDoSzyfrowania.Name = "rtbTekstDoSzyfrowania";
            this.rtbTekstDoSzyfrowania.Size = new System.Drawing.Size(333, 137);
            this.rtbTekstDoSzyfrowania.TabIndex = 8;
            this.rtbTekstDoSzyfrowania.Text = "";
            // 
            // btnSzyfruj
            // 
            this.btnSzyfruj.Location = new System.Drawing.Point(290, 18);
            this.btnSzyfruj.Name = "btnSzyfruj";
            this.btnSzyfruj.Size = new System.Drawing.Size(75, 23);
            this.btnSzyfruj.TabIndex = 9;
            this.btnSzyfruj.Text = "Szyfruj";
            this.btnSzyfruj.UseVisualStyleBackColor = true;
            this.btnSzyfruj.Click += new System.EventHandler(this.btnSzyfruj_Click);
            // 
            // btnZapiszDoPliku
            // 
            this.btnZapiszDoPliku.Location = new System.Drawing.Point(536, 18);
            this.btnZapiszDoPliku.Name = "btnZapiszDoPliku";
            this.btnZapiszDoPliku.Size = new System.Drawing.Size(87, 23);
            this.btnZapiszDoPliku.TabIndex = 10;
            this.btnZapiszDoPliku.Text = "Zapisz do Pliku";
            this.btnZapiszDoPliku.UseVisualStyleBackColor = true;
            this.btnZapiszDoPliku.Click += new System.EventHandler(this.btnZapiszDoPliku_Click);
            // 
            // btnWczytajPlik
            // 
            this.btnWczytajPlik.Location = new System.Drawing.Point(452, 18);
            this.btnWczytajPlik.Name = "btnWczytajPlik";
            this.btnWczytajPlik.Size = new System.Drawing.Size(75, 23);
            this.btnWczytajPlik.TabIndex = 11;
            this.btnWczytajPlik.Text = "Wczytaj Plik";
            this.btnWczytajPlik.UseVisualStyleBackColor = true;
            this.btnWczytajPlik.Click += new System.EventHandler(this.btnWczytajPlik_Click);
            // 
            // btnDeszyfruj
            // 
            this.btnDeszyfruj.Location = new System.Drawing.Point(371, 18);
            this.btnDeszyfruj.Name = "btnDeszyfruj";
            this.btnDeszyfruj.Size = new System.Drawing.Size(75, 23);
            this.btnDeszyfruj.TabIndex = 12;
            this.btnDeszyfruj.Text = "Deszyfruj";
            this.btnDeszyfruj.UseVisualStyleBackColor = true;
            this.btnDeszyfruj.Click += new System.EventHandler(this.btnDeszyfruj_Click);
            // 
            // btnCofnij
            // 
            this.btnCofnij.Location = new System.Drawing.Point(146, 31);
            this.btnCofnij.Name = "btnCofnij";
            this.btnCofnij.Size = new System.Drawing.Size(75, 23);
            this.btnCofnij.TabIndex = 13;
            this.btnCofnij.Text = "Cofnij";
            this.btnCofnij.UseVisualStyleBackColor = true;
            this.btnCofnij.Click += new System.EventHandler(this.btnCofnij_Click);
            // 
            // lblNazwaPioseki
            // 
            this.lblNazwaPioseki.AutoSize = true;
            this.lblNazwaPioseki.Location = new System.Drawing.Point(29, 84);
            this.lblNazwaPioseki.Name = "lblNazwaPioseki";
            this.lblNazwaPioseki.Size = new System.Drawing.Size(92, 13);
            this.lblNazwaPioseki.TabIndex = 14;
            this.lblNazwaPioseki.Text = "<nazwa piosenki>";
            // 
            // btnZapiszKlucz
            // 
            this.btnZapiszKlucz.Location = new System.Drawing.Point(225, 59);
            this.btnZapiszKlucz.Name = "btnZapiszKlucz";
            this.btnZapiszKlucz.Size = new System.Drawing.Size(59, 35);
            this.btnZapiszKlucz.TabIndex = 15;
            this.btnZapiszKlucz.Text = "Zapisz Klucz";
            this.btnZapiszKlucz.UseVisualStyleBackColor = true;
            this.btnZapiszKlucz.Click += new System.EventHandler(this.btnZapiszKlucz_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 201);
            this.Controls.Add(this.btnZapiszKlucz);
            this.Controls.Add(this.lblNazwaPioseki);
            this.Controls.Add(this.btnCofnij);
            this.Controls.Add(this.btnDeszyfruj);
            this.Controls.Add(this.btnWczytajPlik);
            this.Controls.Add(this.btnZapiszDoPliku);
            this.Controls.Add(this.btnSzyfruj);
            this.Controls.Add(this.rtbTekstDoSzyfrowania);
            this.Controls.Add(this.lblCzas);
            this.Controls.Add(this.lblKlucz);
            this.Controls.Add(this.btnZłapKlucz);
            this.Controls.Add(this.btnWczytajPiosenke);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.tbCzas);
            this.Name = "Menu";
            this.Text = "Symfoszyfr";
            ((System.ComponentModel.ISupportInitialize)(this.tbCzas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar tbCzas;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnWczytajPiosenke;
        private System.Windows.Forms.Button btnZłapKlucz;
        private System.Windows.Forms.Label lblKlucz;
        private System.Windows.Forms.Label lblCzas;
        private System.Windows.Forms.RichTextBox rtbTekstDoSzyfrowania;
        private System.Windows.Forms.Button btnSzyfruj;
        private System.Windows.Forms.Button btnZapiszDoPliku;
        private System.Windows.Forms.Button btnWczytajPlik;
        private System.Windows.Forms.Button btnDeszyfruj;
        private System.Windows.Forms.Button btnCofnij;
        private System.Windows.Forms.Label lblNazwaPioseki;
        private System.Windows.Forms.Button btnZapiszKlucz;
    }
}

